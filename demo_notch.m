[originale, Fs] = audioread('la-espero.flac');
originale=originale(:)';

T = 1/Fs;

f_0 = 500;
r=0.99;
f_0_norm = f_0 / Fs;

t=(0:(length(originale)-1)) * T;

con_disturbo = originale + 0.4*cos(2*pi*f_0*t);

num = [1, -2*cos(2*pi*f_0_norm), 1];

den = [1, -2*r*cos(2*pi*f_0_norm), r*r];
figure(1)
zplane(num,1);
title('Solo zeri');

figure(2)
freqz(num, 1, [], Fs);
title('Solo zeri');

figure(3)
zplane(num,den);
title('Zeri e poli');

figure(4)
freqz(num, den, [], Fs);
title('Zeri e poli');


pause

soli_zeri = filter(num, 1, con_disturbo);

full_filter = filter(num, den, con_disturbo);

sound(originale, Fs)

sound(con_disturbo, Fs)

sound(soli_zeri, Fs)

sound(full_filter, Fs)

sound(originale, Fs)
